set -axe
CONTAINER_NAME=$1
if [ ! "$(docker ps -f name="$CONTAINER_NAME" --format "{{.Ports}}")" ]; then
  echo "Creating container $CONTAINER_NAME..."
  docker run -d --rm --name "$CONTAINER_NAME" -p 8080 -w /opt/curso-gitlab-ci -v "$(pwd)":/opt/curso-gitlab-ci curso-gitlab-ci ./run.sh
  docker ps | grep "$CONTAINER_NAME"
else
  echo "Updating container..."
  git pull origin "$CONTAINER_NAME"
fi